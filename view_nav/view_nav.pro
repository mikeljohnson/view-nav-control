QT += qml quick dbus

DBUS_ADAPTORS += view_nav.xml
HEADERS += view_nav.h
SOURCES += view_nav.cpp main.cpp
RESOURCES += assets.qrc

target.path = .
INSTALLS += target
