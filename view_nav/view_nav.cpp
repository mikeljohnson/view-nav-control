//
//     Copyright (C) 2020 Mikel Johnson <mikel5764@gmail.com> 
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#include "view_nav.h"
#include <QVariant>

ViewNavigator::ViewNavigator()
{
    this->setProperty("CanGoBackward", false);
    this->setProperty("CanGoForward", false);
}

void ViewNavigator::setCanGoBackward(const bool &canGoBackward){
    if(canGoBackward != m_backward){
        m_backward = canGoBackward;
        this->setProperty("CanGoBackward", canGoBackward);
        emit CanGoBackwardChanged(m_backward);
    }
};

void ViewNavigator::setCanGoForward(const bool &canGoForward){
    if(canGoForward != m_forward){
        m_forward = canGoForward;
        this->setProperty("CanGoForward", canGoForward);
        emit CanGoForwardChanged(m_forward);
    }
};

void ViewNavigator::goBackward()
{
    emit requestedToGoBackward();
}

void ViewNavigator::goForward()
{
    emit requestedToGoForward();
}
