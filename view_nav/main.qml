//
//     Copyright (C) 2020 Mikel Johnson <mikel5764@gmail.com> 
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

ApplicationWindow {
    id: window
    title: qsTr("View navigation")
    width: 640
    height: 480
    visible: true

    property int stackItems: 1
    onStackItemsChanged:{
        stack.currentIndex = window.stackItems-1
    }
    
    property bool canGoBackward: stack.currentIndex >= 1
    property bool canGoForward: stack.currentIndex < window.stackItems-1

    onCanGoBackwardChanged: {
        viewNavigator.setCanGoBackward(canGoBackward)
        viewNavigator.setCanGoForward(canGoForward)
    }
    onCanGoForwardChanged: {
        viewNavigator.setCanGoBackward(canGoBackward)
        viewNavigator.setCanGoForward(canGoForward)
    }

    Connections {
        target: viewNavigator
        onRequestedToGoBackward:{
            if (window.canGoBackward)
                stack.currentIndex--;
        }
        onRequestedToGoForward:{
            if (window.canGoForward)
                stack.currentIndex++;
        }
    }

    StackLayout {
        id: imageStack
        anchors.fill: parent
        currentIndex: stack.currentIndex
        Repeater{
            model: 10
            Image {
                fillMode: Image.PreserveAspectCrop
                source: "https://source.unsplash.com/random/640x480?sig="+index
            }
        }
    }
    
    StackLayout {
        id: stack
        anchors.fill: parent
        Repeater{
            model: window.stackItems
            Item{
                Column {
                    anchors.centerIn: parent
                    spacing: 10
                    Label {
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: "PAGE #" + index + index + index + index + index
                        color:"white"
                        bottomInset: -5; topInset: -5; leftInset: -5; rightInset: -5
                        background: Rectangle { color:"#55000000"; radius: 2 }
                    }
                    Button {
                        text: "Child view"
                        anchors.horizontalCenter: parent.horizontalCenter
                        visible: stack.currentIndex < 9
                        onClicked: {
                            if(window.stackItems-1 <= stack.currentIndex)
                                window.stackItems++;
                            else
                                stack.currentIndex++;
                        }
                    }
                }
            }
        }
    }

    header: ToolBar {
        Row {
            anchors.fill: parent
            spacing: 10
            ToolButton {
                text: qsTr("ᐊ")
                enabled: window.canGoBackward
                onClicked: stack.currentIndex--;
            }
            ToolButton {
                text: qsTr("ᐅ")
                enabled: window.canGoForward
                onClicked: stack.currentIndex++;
            }
            ToolButton{
                text: qsTr("⭮")
                enabled: window.stackItems > 1
                onClicked: window.stackItems = 1
            }
        }
        Label {
            text: "Title"
            elide: Label.ElideRight
            anchors.centerIn:parent
            Layout.fillWidth: true
        }
    }
} 
