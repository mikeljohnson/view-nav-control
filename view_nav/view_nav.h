//
//     Copyright (C) 2020 Mikel Johnson <mikel5764@gmail.com> 
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#ifndef VIEWNAV_H
#define VIEWNAV_H

#include <QObject>

class ViewNavigator : public QObject
{
    Q_OBJECT
public:
    ViewNavigator();
    Q_INVOKABLE void setCanGoBackward(const bool &canGoBackward);
    Q_INVOKABLE void setCanGoForward(const bool &canGoForward);

public slots:
    void goBackward();
    void goForward();

signals:
    void CanGoBackwardChanged(bool in0);
    void CanGoForwardChanged(bool in0);
    void requestedToGoBackward();
    void requestedToGoForward();

private:
    bool m_forward = false;
    bool m_backward = false;
};

#endif // VIEWNAV_H
