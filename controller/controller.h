//
//     Copyright (C) 2020 Mikel Johnson <mikel5764@gmail.com> 
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "view_nav_interface.h"

class Controller : public QObject
{
    Q_OBJECT

public:
    Controller();
    Q_INVOKABLE void goBackward();
    Q_INVOKABLE void goForward();

signals:
    void canGoBackwardChanged(bool in0);
    void canGoForwardChanged(bool in0);
    void connectedChanged(bool connected);

protected:
    void timerEvent(QTimerEvent *event);
    bool m_connected = false;

public slots:
    void relayCanGoBackward(bool in0);
    void relayCanGoForward(bool in0);

private:
    org::ViewNavigator *viewNavigator;
};

#endif

