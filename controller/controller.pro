QT += qml quick dbus

DBUS_INTERFACES += view_nav.xml
HEADERS += controller.h
SOURCES += main.cpp controller.cpp
RESOURCES += assets.qrc

target.path = .
INSTALLS += target
