//
//     Copyright (C) 2020 Mikel Johnson <mikel5764@gmail.com> 
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#include <QtQuick/qquickview.h>
#include <QQmlApplicationEngine> 
#include <QGuiApplication>
#include <qqmlengine.h>
#include <qqmlcontext.h>
#include <QtQuick/qquickitem.h>
#include <qqml.h>
#include <QtDBus>
#include "controller.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    app.setWindowIcon(QIcon::fromTheme("input-gamepad"));
    
    Controller controller;

    QQmlApplicationEngine view;

    view.rootContext()->setContextProperty("controller", &controller);

    view.load("main.qml");

    return app.exec();
}
