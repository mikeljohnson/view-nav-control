//
//     Copyright (C) 2020 Mikel Johnson <mikel5764@gmail.com> 
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

ApplicationWindow {
    id: window
    title: qsTr("View navigation controller")
    width: 250
    height: 75
    minimumWidth: 200
    minimumHeight: 50
    visible: true
    Connections {
        target: controller
        onCanGoBackwardChanged:{
            backButton.enabled = in0
                
        }
        onCanGoForwardChanged:{
            forwardButton.enabled = in0    
        }
        onConnectedChanged:{
            connectionDisplay.text = connected ? "Connected" : "No connection"
            if(!connected){
                backButton.enabled = false
                forwardButton.enabled = false
            }
        }
    }
    Row {
        anchors.centerIn: parent
        height: parent.height
        spacing: 10
        ToolButton {
            id: backButton
            anchors.verticalCenter: parent.verticalCenter
            text: qsTr("ᐊ")
            enabled: false
            onClicked: controller.goBackward()
        }
        Text{
            id: connectionDisplay
            height: parent.height
            verticalAlignment: Text.AlignVCenter
            text: "No connection"
        }
        ToolButton {
            id: forwardButton
            anchors.verticalCenter: parent.verticalCenter
            text: qsTr("ᐅ")
            enabled: false
            onClicked: controller.goForward()
        }
    }
} 
