//
//     Copyright (C) 2020 Mikel Johnson <mikel5764@gmail.com> 
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#include "controller.h"
#include "view_nav_interface.h"

Controller::Controller()
{
    viewNavigator = new org::ViewNavigator("org.ViewNavigator.AppExample", "/ViewNavigator",
                           QDBusConnection::sessionBus(), this);
    connect(viewNavigator, &org::ViewNavigator::CanGoBackwardChanged, this, &Controller::relayCanGoBackward);
    connect(viewNavigator, &org::ViewNavigator::CanGoForwardChanged, this, &Controller::relayCanGoForward);
    startTimer(1000);
}

void Controller::relayCanGoBackward(bool in0)
{
    emit canGoBackwardChanged(in0);
}

void Controller::relayCanGoForward(bool in0)
{
    emit canGoForwardChanged(in0);
}

void Controller::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event);
    bool old_connected = m_connected;
    m_connected = viewNavigator->isValid();
    if(old_connected != m_connected){
        emit connectedChanged(m_connected);
        if(m_connected){
            emit canGoBackwardChanged(viewNavigator->property("CanGoBackward").toBool());
            emit canGoForwardChanged(viewNavigator->property("CanGoForward").toBool());
        }
    }
}

void Controller::goBackward()
{
    viewNavigator->goBackward();
}

void Controller::goForward()
{
    viewNavigator->goForward();
}
